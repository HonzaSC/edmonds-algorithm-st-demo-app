import * as d3 from 'd3';
import download from 'js-file-download';
import GraphVisualizer from './GraphVisualizer';
import { edmond } from './graphFunctions';
import { getGraphFromGraphML, getGraphMLFromGraph } from './graphMLFunctions';
import { log, LVL_INFO } from './console';

const testGraph = {
  nodes: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }, { id: 6 }, { id: 7 }, { id: 8 }],
  edges: [
    { source: 1, target: 3, weight: 6 },
    { source: 1, target: 3, weight: 16 },
    { source: 3, target: 8, weight: 3 },
    { source: 6, target: 8, weight: 3 },
    { source: 1, target: 2, weight: 5 },
    { source: 4, target: 3, weight: 4 },
    { source: 5, target: 6, weight: 4 },
    { source: 8, target: 7, weight: 5 },
    { source: 2, target: 4, weight: 4 },
    { source: 4, target: 7, weight: 3 },
    { source: 7, target: 5, weight: 2 },
    { source: 5, target: 2, weight: 1 },
    { source: 5, target: 5, weight: 1 },
  ],
  root: null,
};

const state = {
  graph: testGraph,
  buffer: [],
  index: -1,
};

const getGraphForVisualizerFromModel = graph => ({
  nodes: graph.nodes,
  edges: graph.edges.map((edge, id) => ({
    ...edge,
    id,
    source: graph.nodes.find(node => node.id === edge.source),
    target: graph.nodes.find(node => node.id === edge.target),
  })),
});

const getGraphModelFromVisualizer = graph => ({
  nodes: graph.nodes,
  root: graph.root ? graph.root.id : null,
  edges: graph.edges.map(edge => ({
    source: edge.source.id,
    target: edge.target.id,
    weight: edge.weight,
  })),
});

const downloadGraph = (graph, name) => {
  const xml = getGraphMLFromGraph(graph);

  download(xml, name);
};

const runEdmond = (inputGraph) => {
  if (!inputGraph.root) {
    log('Nebyl zvolen kořenový uzel.', LVL_INFO);
    return [];
  }
  const buffer = [];

  edmond(inputGraph, buffer);

  return buffer;
};

const updateGraphVisualization = (graphVisualizer, graph) => {
  graphVisualizer.setGraph(getGraphForVisualizerFromModel(graph));
  graphVisualizer.updateForce();
  graphVisualizer.updateGraph();
};

const clearResult = () => {
  d3.selectAll('.result').remove();
};

const drawBuffer = (inputGraph, buffer) => {
  clearResult();

  const nodesPositionMap = new Map();

  inputGraph.nodes.forEach((node) => {
    nodesPositionMap.set(node.id, { x: node.x, y: node.y });
  });

  const d3Results = d3.select('.results');

  buffer
    .map(({ title, graph }) => ({
      title,
      graph: {
        ...graph,
        nodes: graph.nodes.map((node) => {
          if (node.replace) {
            const x =
              node.replace.reduce(
                (acc, replaceNode) => acc + nodesPositionMap.get(replaceNode.id).x,
                0,
              ) / node.replace.length;
            const y =
              node.replace.reduce(
                (acc, replaceNode) => acc + nodesPositionMap.get(replaceNode.id).y,
                0,
              ) / node.replace.length;

            nodesPositionMap.set(node.id, { x, y });
            return { ...node, x, y };
          }
          return {
            ...node,
            x: nodesPositionMap.get(node.id).x,
            y: nodesPositionMap.get(node.id).y,
          };
        }),
      },
    }))
    .forEach(({ title, graph }) => {
      const d3ResultGraph = d3Results.append('div').classed('result', true);

      const d3SvgWrapper = d3ResultGraph.append('div').classed('result-graph', true);
      const boxSpec = d3SvgWrapper.node().getBoundingClientRect();

      const d3ResultSvg = d3SvgWrapper
        .append('svg')
        .attr('width', boxSpec.width)
        .attr('height', boxSpec.height)
        .attr('viewBox', `0 0 ${boxSpec.width} ${boxSpec.height}`);
      const resultGraphVisualizer = new GraphVisualizer(
        d3ResultSvg,
        getGraphForVisualizerFromModel(graph),
      );
      resultGraphVisualizer.updateGraph();

      const description = d3ResultGraph.append('div').classed('description', true);

      description.append('p').text(title);

      description
        .append('i')
        .classed('fa fa-download', true)
        .attr('aria-hidden', 'true')
        .attr('title', 'Export as GraphML')
        .on('click', () => {
          downloadGraph(graph, 'edmonds-step.xml');
        });
    });
};

const updateVisibleSteps = (inputGraph, buffer, index) => {
  if (buffer.length === 0 || index === -1) {
    drawBuffer(inputGraph, []);

    return;
  }

  if (index === buffer.length) {
    drawBuffer(inputGraph, [
      {
        title: 'Výsledkem je minimální kostra zadaného orientovaného grafu.',
        recursion: 0,
        graph: buffer[buffer.length - 1].graph,
      },
    ]);

    return;
  }

  const subBuffer = buffer.slice(0, index + 1);

  const maxRecursion = subBuffer[subBuffer.length - 1].recursion;

  const subSubBuffer = subBuffer.filter(step =>
    step.recursion <= maxRecursion ||
      (step.recursion === maxRecursion + 1 && (step.type === 3 || step.type === 0)));

  const subSubSubBuffer =
    subSubBuffer.length !== subBuffer.length
      ? subSubBuffer.filter(step => !(step.recursion === maxRecursion && step.type === 2))
      : subSubBuffer;

  drawBuffer(inputGraph, subSubSubBuffer);
};

const stepBackward = () => {
  document.getElementById('legend').classList.add('hidden');

  if (state.buffer.length < 1) return;

  if (state.index > -1) {
    state.index -= 1;
  }

  updateVisibleSteps(state.graph, state.buffer, state.index);

  window.scrollTo(window.outerWidth * state.buffer.length, 0);
};

const stepForward = () => {
  document.getElementById('legend').classList.add('hidden');

  if (state.buffer.length < 1) {
    state.buffer = runEdmond(state.graph);
    if (state.buffer.length < 1) {
      document.getElementById('forward-button').classList.remove('active'); // dirty trik ... alert prevents keyup ...
      return;
    }
  }

  if (state.index < state.buffer.length) {
    state.index += 1;
  }

  updateVisibleSteps(state.graph, state.buffer, state.index);

  window.scrollTo(window.outerWidth * state.buffer.length, 0);
};

/** MAIN SVG * */
const d3ControlGraph = d3.select('.input-graph');
const svg = d3ControlGraph
  .append('svg')
  .attr('width', d3ControlGraph.node().getBoundingClientRect().width)
  .attr('height', d3ControlGraph.node().getBoundingClientRect().height);
const graphVisualizer = new GraphVisualizer(svg, getGraphForVisualizerFromModel(testGraph), true);
graphVisualizer.updateForce();
graphVisualizer.updateGraph();
graphVisualizer.onTopologyChange(() => {
  const graph = graphVisualizer.getGraph();

  state.graph = getGraphModelFromVisualizer(graph);
  state.buffer = [];
  state.index = -1;

  updateVisibleSteps(state.graph, state.buffer, state.index);
});

document.getElementById('legend-link').onclick = (event) => {
  event.preventDefault();

  state.buffer = [];
  state.index = -1;

  updateVisibleSteps(state.graph, state.buffer, state.index);

  document.getElementById('legend').classList.toggle('hidden');
};

document.getElementById('back-button').onclick = () => {
  stepBackward();
};

document.getElementById('forward-button').onclick = () => {
  stepForward();
};

document.getElementById('export-input-button').onclick = () => {
  downloadGraph(state.graph, 'edmonds-input.xml');
};

document.getElementById('input-file-button').onclick = () => {
  document.getElementById('input-file').click();
};

document.getElementById('input-file').onchange = (event) => {
  const reader = new FileReader();

  reader.onload = (e) => {
    const xml = e.target.result;

    getGraphFromGraphML(xml).then((graph) => {
      clearResult();

      state.graph = graph;
      state.buffer = [];
      state.index = -1;

      updateGraphVisualization(graphVisualizer, graph);
    });
  };

  reader.readAsText(event.target.files[0]);
};

document.onkeydown = (event) => {
  const rightArrowKey = 39;
  const leftArrowKey = 37;

  if (event.keyCode === leftArrowKey) {
    document.getElementById('back-button').classList.add('active');

    event.preventDefault();

    stepBackward();
  }

  if (event.keyCode === rightArrowKey) {
    document.getElementById('forward-button').classList.add('active');

    stepForward();
  }
};

document.onkeyup = (event) => {
  const rightArrowKey = 39;
  const leftArrowKey = 37;

  if (event.keyCode === leftArrowKey) {
    document.getElementById('back-button').classList.remove('active');

    event.preventDefault();
  }

  if (event.keyCode === rightArrowKey) {
    document.getElementById('forward-button').classList.remove('active');
  }
};
