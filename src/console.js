export const LVL_DEBUG = 0;
export const LVL_INFO = 1;
export const LVL_WARNING = 2;
export const LVL_ERROR = 3;

const logToConsole = (object, level) => {
  switch (level) {
    case LVL_ERROR:
      console.error(object); // eslint-disable-line
      break;
    case LVL_WARNING:
      console.warning(object); // eslint-disable-line
      break;
    default:
      console.log(object); // eslint-disable-line
      break;
  }
};

export const log = (object, level) => {
  logToConsole(object, level);
  if (level > LVL_DEBUG) alert(object); // eslint-disable-line
};

export default log;
