export const isEdgeEqual = (edge1, edge2) =>
  edge1.target === edge2.target && edge1.source === edge2.source && edge1.weight === edge2.weight;

const isNodeEqual = (node1, node2) => node1.id === node2.id;

export const getEdgesWithoutTargetToNode = (edges, nodeId) =>
  edges.filter(edge => edge.target !== nodeId);

export const filterDuplicatedEdges = (edge, index, self) =>
  self.findIndex(edge2 => isEdgeEqual(edge, edge2)) === index;

export const getEdgesWithoutParallelEdgesByWeight = edges =>
  edges
    .filter((edge, index, self) =>
      !self.find(edge2 =>
        edge.source === edge2.source &&
            edge.target === edge2.target &&
            edge2.weight < edge.weight))
    .filter(filterDuplicatedEdges);

export const reduceNodesMaxId = (acc, node) => (acc < node.id ? node.id : acc);

export const getNodeOutgoingEdges = (node, edges) => edges.filter(edge => edge.source === node);

export const cycleEdgesToNodes = edges => edges.map(edge => ({ id: edge.source }));

export const completePath = (list, paths) => {
  const last = list[list.length - 1];

  if (last.source === list[0].target) {
    return list;
  }

  list.push(paths.get(last.source));

  return completePath(list, paths);
};

export const dfs = (node, white, gray, black, paths, { nodes, edges, root }) =>
  getNodeOutgoingEdges(node, edges)
    .map((outgoingEdge) => {
      const follower = outgoingEdge.target;

      paths.set(follower, outgoingEdge);

      if (black.includes(follower)) {
        return [];
      }

      if (gray.includes(follower)) {
        const cycleEdges = completePath([outgoingEdge], paths).reverse();

        return [
          {
            nodes: cycleEdgesToNodes(cycleEdges),
            edges: cycleEdges,
            root: null,
          },
        ];
      }

      if (white.includes(follower)) {
        gray.push(follower);

        const cycles = dfs(follower, white, gray, black, paths, { nodes, edges, root });

        black.push(follower);

        return cycles;
      }

      return [];
    })
    .reduce((flat, sub) => flat.concat(sub), []);

export const getGraphCycles = ({ nodes, edges, root }) => {
  const white = nodes.map(node => node.id);
  const gray = [];
  const black = [];
  const paths = new Map();
  let cycles = [];

  white.forEach((node) => {
    gray.push(node);
    paths.set(node, null);

    cycles = cycles.concat(dfs(node, white, gray, black, paths, { nodes, edges, root }));

    black.push(node);
  });

  return cycles;
};

// Množina P podle Edmonda algoritmu
export const getGraphPObject = (edges) => {
  const P = {};

  edges.forEach((e) => {
    if (!P[e.target] || P[e.target].weight > e.weight) {
      P[e.target] = e;
    }
  });

  return P;
};

const createExpansionMessage = (spanningTree, recursion, pArray) => ({
  title:
    'Vyznačené hrany neobsahovaly cyklus. Tedy našli jsme minimální kostru pro předchozí graf.',
  recursion,
  type: 0,
  graph: {
    ...spanningTree,
    edges: spanningTree.edges.map((edge) => {
      const duplicateEdges = spanningTree.edges.filter(pEdge => isEdgeEqual(pEdge, edge));

      return pArray.find(pEdge => isEdgeEqual(pEdge, edge)) && duplicateEdges.indexOf(edge) === 0
        ? { ...edge, highlight: true }
        : edge;
    }),
  },
});

const createCycleDetectionMessage = (graph, recursion, cycle, pArray) => {
  const cycleNodes = [...cycle.nodes, cycle.nodes[0]].map(node => node.id).concat();

  return {
    title: `Ve vyznačených hranách jsme našli cyklus ${cycleNodes}.`,
    recursion,
    type: 1,
    graph: {
      root: graph.root,
      nodes: graph.nodes.map((node) => {
        if (cycle.nodes.find(cNode => isNodeEqual(cNode, node))) {
          return { ...node, highlight: true };
        }

        return node;
      }),
      edges: graph.edges.map((edge) => {
        const duplicateEdges = graph.edges.filter(pEdge => isEdgeEqual(pEdge, edge));

        return pArray.find(pEdge => isEdgeEqual(pEdge, edge)) && duplicateEdges.indexOf(edge) === 0
          ? { ...edge, highlight: true }
          : edge;
      }),
    },
  };
};

const createCycleContractionMessage = (graphWithoutCycle, recursion, cycleReplacementId) => ({
  title: `Cyklus smrštíme do jediného uzlu ${cycleReplacementId} a váhu každé hrany vedoucí do uzlu cyklu ponížíme o váhu hrany, která byla vyznačená a vstupuje do stejného uzlu.`,
  recursion,
  type: 2,
  graph: graphWithoutCycle,
});

const createCycleExpansionMessage = (spanningTree, recursion, cycle, cycleReplacement) => {
  const cycleNodes = cycle.nodes.map(node => node.id).concat();

  return {
    title: `Uzel ${cycleReplacement} expandujeme na původní cyklus ${cycleNodes}. Tento cyklus je potřeba rozbít. V předchozí minimální kostře vede do uzlu ${cycleReplacement} pouze 1 hrana. K této hraně najdeme původní hranu, která směřuje do jednoho z uzlů v cyklu a tomuto uzlu odstraníme vstupní hranu. Tím je cyklus rozbit. Nyní vezmeme všechny hrany z předchozí z minimální předchozí kostry a nahradíme je jejími původními hranami.`,
    recursion,
    type: 3,
    graph: spanningTree,
  };
};

const incomingEdgesHighligh = (graph, recursion, pArray) => ({
  title: `${recursion === 0
    ? 'Odstraníme hrany, které vstupují do kořenového uzlu. '
    : ''}Pro každý uzel vyznačíme vstupní hranu s nejmenším ohodnocením.`,
  recursion,
  type: 4,
  graph: {
    root: graph.root,
    nodes: graph.nodes,
    edges: graph.edges.map((edge) => {
      const duplicateEdges = graph.edges.filter(pEdge => isEdgeEqual(pEdge, edge));

      return pArray.find(pEdge => isEdgeEqual(pEdge, edge)) && duplicateEdges.indexOf(edge) === 0
        ? { ...edge, highlight: true }
        : edge;
    }),
  },
});

export const edmond = (graph, buffer, recursion = 0) => {
  const { nodes, root } = graph;
  const edges = getEdgesWithoutTargetToNode(graph.edges, root);

  const P = getGraphPObject(edges);
  const pArray = Object.values(P);

  buffer.push(incomingEdgesHighligh({ nodes, root, edges }, recursion, pArray));

  const cycles = getGraphCycles({ nodes, edges: pArray, root });

  if (cycles.length === 0) {
    const spanningTree = { nodes, edges: pArray, root };

    buffer.push(createExpansionMessage({ nodes, edges: pArray, root }, recursion, pArray));

    return spanningTree;
  }

  const cycle = cycles[0];

  buffer.push(createCycleDetectionMessage({ nodes, edges, root }, recursion, cycle, pArray));

  const cycleReplacementId = nodes.reduce(reduceNodesMaxId, 0) + 1;

  const graphWithoutCycle = { nodes: [], root, edges: [] };

  graphWithoutCycle.nodes = [
    ...nodes.filter(node => !cycle.nodes.find(dNode => dNode.id === node.id)),
    {
      id: cycleReplacementId,
      replace: cycle.nodes,
    },
  ];

  graphWithoutCycle.edges = edges.reduce((edgesAccumulator, edge) => {
    const sourceInCycle = cycle.nodes.find(cycleNode => edge.source === cycleNode.id);
    const targetInCycle = cycle.nodes.find(cycleNode => edge.target === cycleNode.id);

    if (!sourceInCycle && targetInCycle) {
      return [
        ...edgesAccumulator,
        {
          source: edge.source,
          target: cycleReplacementId,
          weight: edge.weight - P[edge.target].weight,
          parent: edge,
        },
      ];
    }

    if (sourceInCycle && !targetInCycle) {
      return [
        ...edgesAccumulator,
        {
          source: cycleReplacementId,
          target: edge.target,
          weight: edge.weight,
          parent: edge,
        },
      ];
    }

    if (!sourceInCycle && !targetInCycle) {
      return [
        ...edgesAccumulator,
        {
          ...edge,
          parent: edge,
        },
      ];
    }

    return edgesAccumulator;
  }, []);

  buffer.push(createCycleContractionMessage(graphWithoutCycle, recursion, cycleReplacementId));

  const spanningTreeWithoutCycle = edmond(graphWithoutCycle, buffer, recursion + 1);

  const edgeToCycle = spanningTreeWithoutCycle.edges.find(e => e.target === cycleReplacementId);
  const edgeTargetedToNodeiC = edgeToCycle.parent;
  const removedEdge = P[edgeTargetedToNodeiC.target];

  const spanningTree = {
    nodes,
    edges: [
      ...cycle.edges.filter(e => !isEdgeEqual(e, removedEdge)),
      ...spanningTreeWithoutCycle.edges.map(e => e.parent),
    ],
    root,
  };

  buffer.push(createCycleExpansionMessage(spanningTree, recursion, cycle, cycleReplacementId));

  return spanningTree;
};
