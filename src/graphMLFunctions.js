import { parseString, Builder } from 'xml2js';
import { log, LVL_DEBUG, LVL_ERROR } from './console';

export const getGraphFromGraphML = graphML =>
  new Promise((resolve, reject) => {
    parseString(graphML, (error, result) => {
      if (error) reject(error);
      else resolve(result);
    });
  })
    .then((json) => {
      if (!json.graphml) {
        throw new Error('Error bad XML format - no graphml node found');
      }
      if (!json.graphml.graph) {
        throw new Error('Error bad XML format - no graphml.graph node found');
      }
      const graphJson = json.graphml.graph[0];
      return {
        root: null,
        edges: graphJson.edge.map((edge) => {
          const source = parseInt(edge.$.source, 10);
          const target = parseInt(edge.$.target, 10);
          const weight = parseInt(edge.$.weight, 10);
          if (Number.isNaN(source) || Number.isNaN(target) || Number.isNaN(weight)) {
            throw 42; //eslint-disable-line
          }
          return {
            // id: edge.$.id,
            source,
            target,
            weight,
          };
        }),
        nodes: graphJson.node.map((node) => {
          const id = parseInt(node.$.id, 10);
          if (Number.isNaN(id)) {
            throw 42; //eslint-disable-line
          }
          return {
            id,
          };
        }),
      };
    })
    .catch((e) => {
      if (e === 42) {
        log('Chyba parsování XML, id musí být číselné.', LVL_ERROR);
      } else {
        log('Chyba parsování XML.', LVL_ERROR);
        log(e, LVL_DEBUG);
      }
    });

export const getGraphMLFromGraph = (graph) => {
  const json = {
    graphml: {
      $: {
        xmlns: 'http://graphml.graphdrawing.org/xmlns',
        'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:schemaLocation': 'http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd',
      },
      graph: {
        node: graph.nodes.map(({ id }) => ({
          $: {
            id,
          },
        })),
        edge: graph.edges.map(({ source, target, weight }) => ({
          $: {
            source,
            target,
            weight,
          },
        })),
      },
    },
  };
  const builder = new Builder();
  return builder.buildObject(json);
};
