import * as gF from './graphFunctions';

test('Test getEdgesWithoutTargetToNode', () => {
  const edges = [
    { source: 1, target: 2, weight: 5 },
    { source: 1, target: 2, weight: 15 },
    { source: 2, target: 1, weight: 5 },
    { source: 2, target: 3, weight: 5 },
  ];
  const nodeId = 1;
  const edgesAfter = [
    { source: 1, target: 2, weight: 5 },
    { source: 1, target: 2, weight: 15 },
    { source: 2, target: 3, weight: 5 },
  ];
  expect(gF.getEdgesWithoutTargetToNode(edges, nodeId)).toEqual(edgesAfter);
});

test('Test filterDuplicatedEdges', () => {
  const edges = [
    { source: 1, target: 2, weight: 5 },
    { source: 1, target: 2, weight: 15 },
    { source: 1, target: 2, weight: 5 },
    { source: 2, target: 3, weight: 5 },
  ];
  const edgesAfter = [
    { source: 1, target: 2, weight: 5 },
    { source: 1, target: 2, weight: 15 },
    { source: 2, target: 3, weight: 5 },
  ];
  expect(edges.filter(gF.filterDuplicatedEdges)).toEqual(edgesAfter);
});

test('Test filterParallelEdgesByWeight', () => {
  const edges = [
    { source: 1, target: 2, weight: 5 },
    { source: 1, target: 2, weight: 5 },
    { source: 1, target: 2, weight: 15 },
    { source: 1, target: 2, weight: 50 },
  ];
  const edgesAfter = [{ source: 1, target: 2, weight: 5 }];
  expect(gF.getEdgesWithoutParallelEdgesByWeight(edges)).toEqual(edgesAfter);
});

test('Hrany vychazejici z vrcholu', () => {
  const edges = [
    { source: 1, target: 2, weight: 1 },
    { source: 1, target: 3, weight: 1 },
    { source: 3, target: 4, weight: 1 },
  ];

  const outgoingEdges = gF.getNodeOutgoingEdges(1, edges);

  expect(outgoingEdges).toContainEqual({ source: 1, target: 2, weight: 1 });
  expect(outgoingEdges).toContainEqual({ source: 1, target: 3, weight: 1 });
});

test('Hrany cyklu na vrcholy', () => {
  const edges = [
    { source: 2, target: 3, weight: 1 },
    { source: 3, target: 4, weight: 1 },
    { source: 4, target: 2, weight: 1 },
  ];

  expect(gF.cycleEdgesToNodes(edges)).toEqual([{ id: 2 }, { id: 3 }, { id: 4 }]);
});

test('Nalezeni jednoho cyklu v grafu', () => {
  const graph = {
    nodes: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }],
    edges: [
      { source: 1, target: 2, weight: 1 },
      { source: 2, target: 3, weight: 1 },
      { source: 3, target: 4, weight: 1 },
      { source: 3, target: 5, weight: 1 },
      { source: 4, target: 2, weight: 1 },
    ],
    root: 4,
  };

  const cycle = {
    nodes: [{ id: 2 }, { id: 3 }, { id: 4 }],
    edges: [
      { source: 2, target: 3, weight: 1 },
      { source: 3, target: 4, weight: 1 },
      { source: 4, target: 2, weight: 1 },
    ],
    root: null,
  };

  expect(gF.getGraphCycles(graph)).toContainEqual(cycle);
});

test('Nalezeni vice cyklu v grafu', () => {
  const graph = {
    nodes: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }],
    edges: [
      { source: 1, target: 1, weight: 1 },
      { source: 1, target: 2, weight: 1 },
      { source: 2, target: 3, weight: 1 },
      { source: 3, target: 1, weight: 1 },
      { source: 1, target: 4, weight: 1 },
      { source: 4, target: 5, weight: 1 },
      { source: 5, target: 1, weight: 1 },
    ],
    root: 1,
  };

  const cycle1 = {
    nodes: [{ id: 1 }],
    edges: [{ source: 1, target: 1, weight: 1 }],
    root: null,
  };

  const cycle2 = {
    nodes: [{ id: 1 }, { id: 2 }, { id: 3 }],
    edges: [
      { source: 1, target: 2, weight: 1 },
      { source: 2, target: 3, weight: 1 },
      { source: 3, target: 1, weight: 1 },
    ],
    root: null,
  };

  const cycle3 = {
    nodes: [{ id: 1 }, { id: 4 }, { id: 5 }],
    edges: [
      { source: 1, target: 4, weight: 1 },
      { source: 4, target: 5, weight: 1 },
      { source: 5, target: 1, weight: 1 },
    ],
    root: null,
  };

  const cycles = gF.getGraphCycles(graph);

  expect(cycles).toContainEqual(cycle1);
  expect(cycles).toContainEqual(cycle2);
  expect(cycles).toContainEqual(cycle3);
});

test('Detekce grafu bez cyklu', () => {
  const graph = {
    nodes: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }],
    edges: [
      { source: 1, target: 2, weight: 1 },
      { source: 2, target: 3, weight: 1 },
      { source: 1, target: 4, weight: 1 },
      { source: 4, target: 5, weight: 1 },
    ],
    root: 1,
  };

  const cycles = gF.getGraphCycles(graph);

  expect(cycles.length).toBe(0);
});

test('Test reduceNodesMaxId', () => {
  const nodes = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }];

  expect(nodes.reduce(gF.reduceNodesMaxId, 0)).toBe(5);
});

test('Tvorba mnoziny P', () => {
  const edges = [
    { source: 2, target: 1, weight: 4 },
    { source: 1, target: 3, weight: 4 },
    { source: 7, target: 2, weight: 2 },
    { source: 3, target: 4, weight: 10 },
    { source: 3, target: 5, weight: 5 },
    { source: 5, target: 3, weight: 3 },
    { source: 4, target: 5, weight: 12 },
    { source: 6, target: 4, weight: 9 },
    { source: 7, target: 6, weight: 5 },
    { source: 7, target: 7, weight: 5 },
  ];

  const P = [
    { source: 2, target: 1, weight: 4 },
    { source: 7, target: 2, weight: 2 },
    { source: 5, target: 3, weight: 3 },
    { source: 6, target: 4, weight: 9 },
    { source: 3, target: 5, weight: 5 },
    { source: 7, target: 6, weight: 5 },
    { source: 7, target: 7, weight: 5 },
  ];
  const resultP = gF.getGraphPObject(edges);
  expect(Object.values(resultP)).toEqual(P);
});

test('1-edmond test', () => {
  const graphPrev = {
    nodes: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }, { id: 6 }, { id: 7 }],
    edges: [
      { source: 2, target: 1, weight: 4 },
      { source: 1, target: 3, weight: 4 },
      { source: 7, target: 2, weight: 2 },
      { source: 3, target: 4, weight: 10 },
      { source: 3, target: 5, weight: 5 },
      { source: 5, target: 3, weight: 3 },
      { source: 4, target: 5, weight: 12 },
      { source: 6, target: 4, weight: 9 },
      { source: 7, target: 6, weight: 5 },
      { source: 1, target: 1, weight: 1 },
    ],
    root: 7,
  };
  const graphNext = {
    nodes: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }, { id: 6 }, { id: 7 }],
    edges: [
      { source: 3, target: 5, weight: 5 },
      { source: 7, target: 2, weight: 2 },
      { source: 6, target: 4, weight: 9 },
      { source: 7, target: 6, weight: 5 },
      { source: 2, target: 1, weight: 4 },
      { source: 1, target: 3, weight: 4 },
    ],
    root: 7,
  };
  const result = gF.edmond(graphPrev, []);
  expect(result).toEqual(graphNext);
});

/*
test('Test edmoda rychlost', () => {
  const graphPrev = {
    nodes: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }, { id: 6 }, { id: 7 }],
    edges: [
      { source: 2, target: 1, weight: 4 },
      { source: 1, target: 3, weight: 4 },
      { source: 7, target: 2, weight: 2 },
      { source: 3, target: 4, weight: 10 },
      { source: 3, target: 5, weight: 5 },
      { source: 5, target: 3, weight: 3 },
      { source: 4, target: 5, weight: 12 },
      { source: 6, target: 4, weight: 9 },
      { source: 7, target: 6, weight: 5 },
      //  { source: 1, target: 1, weight: 1 },
    ],
    root: 7,
  };
  const graphNext = {
    nodes: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }, { id: 6 }, { id: 7 }],
    edges: [
      { source: 3, target: 5, weight: 5 },
      { source: 2, target: 1, weight: 4 },
      { source: 7, target: 2, weight: 2 },
      { source: 6, target: 4, weight: 9 },
      { source: 7, target: 6, weight: 5 },
      { source: 1, target: 3, weight: 4 },
    ],
    root: 7,
  };

  let timeResult = 0;
  for (let i = 0; i < 100; i += 1) {
    const time = process.hrtime();
    gF.edmond(graphPrev);
    const time2 = process.hrtime(time);
    timeResult += time2[0] * 1e9 + time2[1];
  }
  console.log(timeResult / 100);
});
*/

test('2-edmond test', () => {
  const graphPrev = {
    nodes: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }, { id: 6 }, { id: 7 }, { id: 8 }],
    edges: [
      { source: 1, target: 3, weight: 6 },
      { source: 1, target: 6, weight: 6 },
      { source: 3, target: 8, weight: 3 },
      { source: 6, target: 8, weight: 3 },
      { source: 1, target: 2, weight: 5 },
      { source: 4, target: 3, weight: 4 },
      { source: 5, target: 6, weight: 4 },
      { source: 8, target: 7, weight: 5 },
      { source: 2, target: 4, weight: 4 },
      { source: 4, target: 7, weight: 3 },
      { source: 7, target: 5, weight: 2 },
      { source: 5, target: 2, weight: 1 },
    ],
    root: 1,
  };
  const sortFunction = (a, b) =>
    parseInt(`${a.source}${a.target}${a.weight}`, 10) <
    parseInt(`${b.source}${b.target}${b.weight}`, 10);

  const reduceWeight = (acc, edge) => acc + edge.weight;
  const exceptedEdges = [
    { source: 8, target: 7, weight: 5 },
    { source: 7, target: 5, weight: 2 },
    { source: 5, target: 6, weight: 4 },
    { source: 5, target: 2, weight: 1 },
    { source: 3, target: 8, weight: 3 },
    { source: 2, target: 4, weight: 4 },
    { source: 1, target: 3, weight: 6 },
  ].sort(sortFunction);
  const resultEdges = gF.edmond(graphPrev, []).edges.sort(sortFunction);
  expect(resultEdges.reduce(reduceWeight, 0)).toEqual(exceptedEdges.reduce(reduceWeight, 0));
  expect(resultEdges).toEqual(exceptedEdges);
});

test('Cyklus v nedostupnych uzlech', () => {
  const graph = {
    nodes: [{ id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }, { id: 6 }, { id: 7 }, { id: 8 }],
    edges: [
      { source: 7, target: 2, weight: 2 },
      { source: 5, target: 3, weight: 3 },
      { source: 6, target: 4, weight: 9 },
      { source: 3, target: 5, weight: 5 },
      { source: 7, target: 6, weight: 5 },
      { source: 2, target: 8, weight: 3 },
    ],
    root: 7,
  };

  const cycle = {
    nodes: [{ id: 3 }, { id: 5 }],
    edges: [{ source: 3, target: 5, weight: 5 }, { source: 5, target: 3, weight: 3 }],
    root: null,
  };

  expect(gF.getGraphCycles(graph)).toContainEqual(cycle);
});

test('3-edmond test', () => {
  const graphPrev = {
    nodes: [{ id: 1 }, { id: 2 }, { id: 3 }],
    edges: [
      { source: 1, target: 2, weight: 2 },
      { source: 2, target: 3, weight: 3 },
      { source: 3, target: 2, weight: 4 },
      { source: 2, target: 2, weight: 1 },
      { source: 3, target: 3, weight: 1 },
    ],
    root: 1,
  };
  const exceptedEdges = [{ source: 1, target: 2, weight: 2 }, { source: 2, target: 3, weight: 3 }];
  const reduceWeight = (acc, edge) => acc + edge.weight;
  const resultEdges = gF.edmond(graphPrev, []).edges;
  expect(resultEdges.reduce(reduceWeight, 0)).toEqual(exceptedEdges.reduce(reduceWeight, 0));
  expect(resultEdges).toEqual(exceptedEdges);
});
