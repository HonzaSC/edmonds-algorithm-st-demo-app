import * as d3 from 'd3'; // eslint-disable-line
import { event as currentEvent } from 'd3'; // eslint-disable-line
import { reduceNodesMaxId } from './graphFunctions';

export default class GraphVisualizer {
  constructor(svg, graph, active = false) {
    this.id = Math.random()
      .toString(36)
      .substring(7);
    this.active = active;
    this.nodeIdCounter = 0;
    this.edgeIdCounter = 0;
    this.d3Svg = svg;
    this.getCurvePath = this.getCurvePath.bind(this);
    this.onTopologyChangeCallback = null;

    this.state = {
      selectedNode: null,
      mouseDownNode: null,
      justDragged: false,
      mouseDownEdge: null,
      selectedEdge: null,
      graphMouseDown: false,
    };
    this.setGraph(graph);
    this.prepareWindow()
      .prepareSvg()
      .prepareDefs()
      .prepareSvgG()
      .prepareDragPath()
      .prepareEdges()
      .prepareEdgesLabels()
      .prepareNodes()
      .prepareNodeDragBehavior()
      .prepareForce();
  }

  setGraph({ nodes, edges }) {
    this.nodeIdCounter = nodes.reduce(reduceNodesMaxId, 0);
    this.edgeIdCounter = edges.reduce(reduceNodesMaxId, 0);
    this.nodes = nodes;
    this.edges = edges;
    if (this.state.selectedNode) this.removeSelectFromNode();
  }

  onTopologyChange(onTopologyChange) {
    this.onTopologyChangeCallback = onTopologyChange;
  }

  triggerTopologyChange() {
    if (typeof this.onTopologyChangeCallback === 'function') {
      this.onTopologyChangeCallback();
    }
  }

  getGraph() {
    return { nodes: this.nodes, edges: this.edges, root: this.state.selectedNode };
  }

  getNewNodeId() {
    this.nodeIdCounter += 1;
    return this.nodeIdCounter;
  }

  getNewEdgeId() {
    this.edgeIdCounter += 1;
    return this.edgeIdCounter;
  }

  updateForce() {
    this.force.nodes(this.nodes);
    this.force.links(this.edges);
    this.force.start();
  }

  prepareWindow() {
    d3
      .select(window)
      .on(`keydown.${this.id}`, () => {
        if (!this.active) return;
        const { keyCode } = currentEvent;
        const { selectedNode, selectedEdge } = this.state;
        switch (keyCode) {
          case 8: // BACKSPACE_KEY
          case 46: // DeLETE KEY
            currentEvent.preventDefault();
            if (selectedNode) {
              this.nodes.splice(this.nodes.indexOf(selectedNode), 1);
              this.spliceLinksForNode(selectedNode);
              this.state.selectedNode = null;
              this.triggerTopologyChange();
              this.updateGraph();
            }
            if (selectedEdge) {
              this.edges.splice(this.edges.indexOf(selectedEdge), 1);
              this.state.selectedEdge = null;
              this.triggerTopologyChange();
              this.updateGraph();
            }
            break;
          default:
            break;
        }
      })
      .on('keyup', () => {
        // this.d3SvgKeyUp();
      });
    return this;
  }

  prepareSvg() {
    this.d3Svg.on('mousedown', () => {
      if (!this.active) return;
      this.state.graphMouseDown = true;
    });
    this.d3Svg.on('mouseup', () => {
      if (!this.active) return;
      if (this.state.graphMouseDown && currentEvent.shiftKey) {
        // clicked not dragged from svg
        const xycoords = d3.mouse(this.d3SvgG.node());
        const newNode = {
          id: this.getNewNodeId(),
          x: xycoords[0],
          y: xycoords[1],
        };
        this.triggerTopologyChange();
        this.nodes.push(newNode);
        this.updateGraph();
      } else if (this.state.shiftNodeDrag) {
        this.state.shiftNodeDrag = false;
        this.d3DragEdge.classed('hidden', true);
      }
      this.state.graphMouseDown = false;
    });
    return this;
  }

  prepareSvgG() {
    this.d3SvgG = this.d3Svg.append('g').classed('graph', true);
    return this;
  }

  prepareDragPath() {
    this.d3DragEdge = this.d3SvgG
      .append('svg:path')
      .attr('class', 'link dragline hidden')
      .attr('d', 'M0,0L0,0')
      .style('marker-end', 'url(#mark-end-arrow)');
    return this;
  }

  prepareEdges() {
    this.d3Edges = this.d3SvgG
      .append('g')
      .attr('class', 'edges')
      .selectAll('g');
    return this;
  }
  prepareEdgesLabels() {
    this.d3EdgesLabels = this.d3SvgG
      .append('g')
      .attr('class', 'edges-labels')
      .selectAll('g');

    return this;
  }

  prepareNodes() {
    this.d3Nodes = this.d3SvgG
      .append('g')
      .attr('class', 'nodes')
      .selectAll('g');
    return this;
  }

  prepareDefs() {
    const defs = this.d3Svg.append('svg:defs');
    defs
      .append('svg:marker')
      .attr('id', 'end-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', '8')
      .attr('markerWidth', 4.5)
      .attr('markerHeight', 4.5)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M0,-5L10,0L0,5');
    defs
      .append('svg:marker')
      .attr('id', 'mark-end-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 7)
      .attr('markerWidth', 3.5)
      .attr('markerHeight', 3.5)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M0,-5L10,0L0,5');
    return this;
  }

  prepareNodeDragBehavior() {
    this.nodeDragBehavior = d3.behavior
      .drag()
      .origin(node => ({ x: node.x, y: node.y }))
      .on('drag', (node) => {
        this.state.justDragged = true;
        // We are moving the node we are not creatig new ege
        if (!this.state.shiftNodeDrag) {
          /* Problem ptom s viewBox neni treba resit
          if (currentEvent.dx < 0) {
            if (node.x > 60) node.x += currentEvent.dx;
          } else if (node.x < this.d3Svg.attr('width') - 60) {
            node.x += currentEvent.dx;
          }
          if (currentEvent.dy < 0) {
            if (node.y > 60) node.y += currentEvent.dy;
          } else if (node.y < this.d3Svg.attr('height') - 60) {
            node.y += currentEvent.dy;
          }
          */
          node.x += currentEvent.dx;
          node.y += currentEvent.dy;
          this.updateGraph();
        } else {
          //  Creating new edge moving
          this.d3DragEdge.attr(
            'd',
            `M${node.x},${node.y}L${d3.mouse(this.d3SvgG.node())[0]},${d3.mouse(this.d3SvgG.node())[1]}`,
          );
        }
      })
      .on('dragend', () => {
        // todo check if edge-mode is selected
      });
    return this;
  }

  prepareForce() {
    const wasGraphActive = this.active;
    this.force = d3.layout
      .force()
      .size([this.d3Svg.attr('width'), this.d3Svg.attr('height')])
      .linkDistance(200)
      .gravity(1)
      .charge(-7000)
      .on('start', () => {
        this.active = false;
      })
      .on('end', () => {
        this.active = wasGraphActive;
      })
      .on('tick', () => {
        this.updateGraph();
      });
  }

  spliceLinksForNode(node) {
    this.edges.filter(edge => edge.source === node || edge.target === node).forEach((edge) => {
      this.edges.splice(this.edges.indexOf(edge), 1);
    });
  }

  circleMouseDown(d3node, node) {
    currentEvent.stopPropagation();
    this.state.mouseDownNode = node;

    if (currentEvent.shiftKey) {
      this.state.shiftNodeDrag = true;
      this.d3DragEdge
        .classed('hidden', false)
        .attr('d', `M${node.x},${node.y}L${node.x},${node.y}`);
    }
  }

  circleMouseUp(d3node, node) {
    if (this.state.shiftNodeDrag) {
      const newEdge = {
        source: this.state.mouseDownNode,
        target: node,
        id: this.getNewEdgeId(),
        weight: 1,
      };
      this.edges.push(newEdge);
      this.triggerTopologyChange();
      this.updateGraph();
    } else if (this.state.mouseDownNode === node) {
      const prevNode = this.state.selectedNode;
      // Clear edge selection
      if (this.state.selectedEdge) {
        this.removeSelectFromEdge();
      }
      // If new nod clicked replace the selectednodes
      if (!prevNode || prevNode.id !== node.id) {
        this.replaceSelectNode(d3node, node);
      } else {
        this.removeSelectFromNode();
      }
      this.triggerTopologyChange();
    }
    this.d3DragEdge.classed('hidden', true);
    this.state.shiftNodeDrag = false;
    this.state.mouseDownNode = null;
  }

  replaceSelectNode(d3Node, node) {
    d3Node.classed('selected', true);
    if (this.state.selectedNode) {
      this.removeSelectFromNode();
    }
    this.state.selectedNode = node;
  }

  removeSelectFromNode() {
    this.d3Nodes
      .filter(d3Edge => d3Edge.id === this.state.selectedNode.id)
      .classed('selected', false);
    this.state.selectedNode = null;
  }

  pathMouseDown(d3path, edge) {
    currentEvent.stopPropagation();
    this.state.mouseDownEdge = edge;

    if (this.state.selectedNode) {
      this.removeSelectFromNode();
      this.triggerTopologyChange();
    }

    const prevEdge = this.state.selectedEdge;
    if (!prevEdge || prevEdge !== edge) {
      this.replaceSelectEdge(d3path, edge);
    } else {
      this.removeSelectFromEdge();
    }
  }

  replaceSelectEdge(d3Path, edge) {
    d3Path.classed('selected', true);
    if (this.state.selectedEdge) {
      this.removeSelectFromEdge();
    }
    this.state.selectedEdge = edge;
  }

  removeSelectFromEdge() {
    this.d3Edges.filter(cd => cd === this.state.selectedEdge).classed('selected', false);
    this.state.selectedEdge = null;
  }

  onEdgeWeightClick(edge) {
    let response;
    // eslint-disable-next-line
    while (true) {
      response = window.prompt('Zadejte váhu hrany:', edge.weight); // eslint-disable-line
      if (response === null) {
        response = edge.weight;
        break;
      }
      response = parseInt(response, 10);
      if (response) break;
    }
    edge.weight = response;
    this.triggerTopologyChange();
    this.updateGraph();
  }

  updateGraph() {
    // circles
    this.d3Nodes = this.d3Nodes.data(this.nodes, node => node.id);
    const newCirclesGs = this.d3Nodes.enter().append('g');
    const thisGraph = this;
    newCirclesGs
      .classed('conceptG', true)
      // eslint-disable-next-line
      .on('mousedown', function(node) {
        if (!thisGraph.active) return;
        thisGraph.circleMouseDown.call(thisGraph, d3.select(this), node);
      })
      // eslint-disable-next-line
      .on('mouseup', function(node) {
        if (!thisGraph.active) return;
        thisGraph.circleMouseUp.call(thisGraph, d3.select(this), node);
      })
      .call(this.nodeDragBehavior);

    newCirclesGs.append('circle').attr('r', '30');
    newCirclesGs.append('text').text(node => node.id);
    this.d3Nodes.exit().remove();
    this.d3Nodes
      .attr('transform', node => `translate(${node.x},${node.y})`)
      .classed('highlight', node => node.highlight);

    // edges

    this.d3Edges = this.d3Edges.data(this.edges, edge => edge.id);
    this.d3Edges
      .enter()
      .append('path')
      .style('marker-end', 'url(#end-arrow)')
      .classed('link', true)
      .classed('highlight', edge => edge.highlight)
      .attr('id', edge => `${this.id}-${edge.id}`)
      // eslint-disable-next-line
      .on('mousedown', function(edge) {
        if (!thisGraph.active) return;
        thisGraph.pathMouseDown.call(thisGraph, d3.select(this), edge);
      })
      .on('mouseup', () => {
        if (!thisGraph.active) return;
        this.state.mouseDownEdge = null;
      });
    this.d3Edges.exit().remove();
    this.d3Edges.attr('d', this.getCurvePath).classed('highlight', edge => edge.highlight);

    this.d3EdgesLabels = this.d3EdgesLabels.data(this.edges, edge => edge.id);
    this.d3EdgesLabels
      .enter()
      .append('text')
      .on('click', (edge) => {
        if (!this.active) return;
        this.onEdgeWeightClick(edge);
      })
      .append('textPath') // append a textPath to the text element
      .attr('xlink:href', edge => `#${this.id}-${edge.id}`) // place the ID of the path here
      .style('text-anchor', 'middle') // place the text halfway on the arc
      .attr('startOffset', '50%')
      .text(edge => edge.weight);
    this.d3EdgesLabels.exit().remove();
    this.d3EdgesLabels.style(
      'dominant-baseline',
      d => (d.target.x < d.source.x ? 'hanging' : 'text-after-edge'),
    );
    this.d3EdgesLabels.selectAll('textPath').text(edge => edge.weight);
    // eslint-disable-next-line
    this.d3EdgesLabels.attr('transform', function(d) {
      if (d.target.x < d.source.x) {
        const bbox = this.getBBox();
        const rx = bbox.x + bbox.width / 2; // eslint-disable-line
        const ry = bbox.y + bbox.height / 2; // eslint-disable-line
        return `rotate(180 ${rx} ${ry})`;
      }
      return 'rotate(0)';
    });

    // this.force.start();
  }

  getCurvePath(edge) {
    if (edge.target.id === edge.source.id) {
      return this.getLoopPath(edge);
    }
    const vec = {
      x: edge.target.x - edge.source.x,
      y: edge.target.y - edge.source.y,
    };
    if (vec.x === 0 || vec.y === 0) {
      return '';
    }
    const vecLength = (vec.x ** 2 + vec.y ** 2) ** 0.5; // eslint-disable-line
    const standardVec = { x: vec.x / vecLength, y: vec.y / vecLength };

    const allEdgesBetwenNodes = this.edges.filter(pE =>
      (pE.source.id === edge.source.id && pE.target.id === edge.target.id) ||
        (pE.source.id === edge.target.id && pE.target.id === edge.source.id));
    if (allEdgesBetwenNodes.length < 2) {
      return `M ${edge.source.x + standardVec.x * 30} ${edge.source.y + // eslint-disable-line
        standardVec.y * 30} L ${edge.target.x - // eslint-disable-line
        standardVec.x * 30} ${edge.target.y - standardVec.y * 30}`; // eslint-disable-line
    }
    // eslint-disable-next-line
    const edgeOrder =
      allEdgesBetwenNodes
        .filter(posibleEdge =>
          posibleEdge.source.id === edge.source.id && posibleEdge.target.id === edge.target.id)
        .indexOf(edge) + 1;
    const controllPointScale = 30 * edgeOrder * 2;

    const controllPoint = {
      x: standardVec.y * controllPointScale + (edge.source.x + edge.target.x) / 2, // eslint-disable-line
      y: -1 * standardVec.x * controllPointScale + (edge.source.y + edge.target.y) / 2, // eslint-disable-line
    };

    return `M ${edge.source.x + standardVec.x * 30} ${edge.source.y + // eslint-disable-line
      standardVec.y * 30} Q ${controllPoint.x} ${controllPoint.y}, ${edge.target.x - // eslint-disable-line
      standardVec.x * 30} ${edge.target.y - standardVec.y * 30}`; // eslint-disable-line
  }

  getLoopPath(edge) {
    const allEdgesBetwenNodes = this.edges.filter(pE =>
      (pE.source.id === edge.source.id && pE.target.id === edge.target.id) ||
        (pE.source.id === edge.target.id && pE.target.id === edge.source.id));
    const edgeOrder = allEdgesBetwenNodes.indexOf(edge) + 1; // Kolikátá hrana v pořadí
    const randomFactor = edge.id % 2 === 1 ? -1 : 1; // Směrovač smyček <- or -> 1:1 podle ID
    // Bodu na obvodu kružnice
    const sourcePoint = {
      x: edge.source.x + 30 * randomFactor, // eslint-disable-line
      y: edge.source.y,
    };
    // eslint-disable-line
    const upControllPoint = {
      x: sourcePoint.x + (100 + edgeOrder * 25) * randomFactor, // eslint-disable-line
      y: sourcePoint.y + (100 + edgeOrder * 25), // eslint-disable-line
    }; // Vynásobení vektorem závyslím na směru smyčky a pořadí smyčky
    // eslint-disable-next-line
    const downControllPoint = {
      x: sourcePoint.x + (100 + edgeOrder * 25) * randomFactor, // eslint-disable-line
      y: sourcePoint.y - (100 + edgeOrder * 25), // eslint-disable-line
    }; // Vynásobení vektorem závyslím na směru smyčky a pořadí smyčky
    return `M ${sourcePoint.x} ${sourcePoint.y} C ${upControllPoint.x} ${upControllPoint.y} ${downControllPoint.x} ${downControllPoint.y}, ${sourcePoint.x} ${sourcePoint.y}`;
  }
}
