import * as gMF from './graphMLFunctions';

test('Test parsování Graph -> GraphML', async () => {
  const graphML = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
      <graphml xmlns="http://graphml.graphdrawing.org/xmlns"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
        <graph>
          <node id="0"/>
          <node id="1"/>
          <edge source="0" target="1" weight="10"/>
        </graph>
      </graphml>`;

  const graph = {
    root: null,
    nodes: [
      {
        id: '0',
      },
      {
        id: '1',
      },
    ],
    edges: [
      {
        source: '0',
        target: '1',
        weight: '10',
      },
    ],
  };
  expect(gMF.getGraphMLFromGraph(graph).replace(/\r?\n|\r|\s/g, '')).toEqual(graphML.replace(/\r?\n|\r|\s/g, ''));
});
