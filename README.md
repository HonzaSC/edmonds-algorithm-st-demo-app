# Edmonds algorithm (ST) demo app #

This app demonstrates how Edmonds algorithm works. It's written as a web app and was develop as a school project at FIT VUT 2017.

# Demo site #
https://edmond.pokornyjan.com/

# Authors #

Jan & Jan Pokorny, Svoboda